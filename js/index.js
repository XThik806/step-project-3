"use strict";

const DATA = [
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m1.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "8 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f1.png",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m2.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Тетяна",
		"last name": "Мороз",
		photo: "./img/trainers/trainer-f2.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
	},
	{
		"first name": "Сергій",
		"last name": "Коваленко",
		photo: "./img/trainers/trainer-m3.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
	},
	{
		"first name": "Олена",
		"last name": "Лисенко",
		photo: "./img/trainers/trainer-f3.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
	},
	{
		"first name": "Андрій",
		"last name": "Волков",
		photo: "./img/trainers/trainer-m4.jpg",
		specialization: "Бійцівський клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
	},
	{
		"first name": "Наталія",
		"last name": "Романенко",
		photo: "./img/trainers/trainer-f4.jpg",
		specialization: "Дитячий клуб",
		category: "спеціаліст",
		experience: "3 роки",
		description:
			"Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
	},
	{
		"first name": "Віталій",
		"last name": "Козлов",
		photo: "./img/trainers/trainer-m5.jpg",
		specialization: "Тренажерний зал",
		category: "майстер",
		experience: "10 років",
		description:
			"Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
	},
	{
		"first name": "Юлія",
		"last name": "Кравченко",
		photo: "./img/trainers/trainer-f5.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
	},
	{
		"first name": "Олег",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-m6.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "12 років",
		description:
			"Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
	},
	{
		"first name": "Лідія",
		"last name": "Попова",
		photo: "./img/trainers/trainer-f6.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
	},
	{
		"first name": "Роман",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m7.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
	},
	{
		"first name": "Анастасія",
		"last name": "Гончарова",
		photo: "./img/trainers/trainer-f7.jpg",
		specialization: "Басейн",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
	},
	{
		"first name": "Валентин",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-m8.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
	},
	{
		"first name": "Лариса",
		"last name": "Петренко",
		photo: "./img/trainers/trainer-f8.jpg",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "7 років",
		description:
			"Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
	},
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m9.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "11 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f9.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m10.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Наталія",
		"last name": "Бондаренко",
		photo: "./img/trainers/trainer-f10.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "8 років",
		description:
			"Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
	},
	{
		"first name": "Андрій",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m11.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
	},
	{
		"first name": "Софія",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-f11.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "6 років",
		description:
			"Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
	},
	{
		"first name": "Дмитро",
		"last name": "Ковальчук",
		photo: "./img/trainers/trainer-m12.png",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
	},
	{
		"first name": "Олена",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-f12.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "5 років",
		description:
			"Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
	},
];

/////////////////////////////////// Функції ////////////////////////////////////
const cardsOut = (outList) => {
	outList.forEach((el) => {
		const template_Clone = document.importNode(template, true);

		let template_IMG = template_Clone.querySelector('.trainer__img');
		let template_Name = template_Clone.querySelector('.trainer__name');

		template_IMG.src = el.photo;
		template_Name.innerText = `${el["first name"]} ${el["last name"]}`;

		cardList.append(template_Clone);
	})
}

const filter = (labelList) => {
	const ways = labelList.querySelectorAll('input');
	let activeWay
	for (let el of ways) {
		if (el.checked) {
			activeWay = el.id;
			break;
		}
	}
	return labelList.querySelector(`label[for=${activeWay}]`).innerText.toLowerCase();
}

////////////////////////////////////////////////////////////////////////////////

const body = document.querySelector('body');

const sorting = document.querySelector('.sorting');
const sidebar = document.querySelector('.sidebar');

setTimeout(() => {
	sorting.removeAttribute("hidden");
	sidebar.removeAttribute("hidden");
	cardsOut(DATA);
}, 2000)

const template = document.getElementById('trainer-card').content;

const cardList = document.querySelector('.trainers-cards__container');



let filterDATA = [...DATA];

document.addEventListener('click', (event) => {
	if (event.target.classList.contains('filters__submit')) {
		filterDATA = [];

		event.preventDefault()
		const filters = event.target.closest('.filters');
		const fieldset = filters.querySelectorAll('.filters__fieldset');

		let speciailization = filter(fieldset[0]);
		let category = filter(fieldset[1]);

		DATA.forEach((el) => {
			if(category === 'всі' && speciailization === 'всі') {
				filterDATA.push(el);
			}

			else if(el.category.toLowerCase() === category && speciailization === 'всі') {
				filterDATA.push(el);
			}

			else if(category === 'всі' && el.specialization.toLowerCase() === speciailization) {
				filterDATA.push(el);
			}

			else if(el.category.toLowerCase() === category && el.specialization.toLowerCase() === speciailization) {
				filterDATA.push(el);
			}
		})

		const cards = cardList.querySelectorAll('.trainer');
		cards.forEach((el) => {
			el.remove();
		})

		cardsOut(filterDATA);

		const sort_btns = document.querySelectorAll('.sorting__btn');
		sort_btns.forEach((el) => {
			if (el.classList.contains('sorting__btn--active')) el.classList.remove('sorting__btn--active');
		})
		sort_btns[0].classList.add('sorting__btn--active');
	}
})


document.addEventListener('click', (event) => {
	if (event.target.classList.contains('trainer__show-more')) {
		const modal_Clone = document.querySelector('#modal-template').content.cloneNode(true);

		let modal_Name = modal_Clone.querySelector('.modal__name');
		let modal_IMG = modal_Clone.querySelector('.modal__img');
		let modal_category = modal_Clone.querySelector('.modal__point--category');
		let modal_experience = modal_Clone.querySelector('.modal__point--experience');
		let modal_specialization = modal_Clone.querySelector('.modal__point--specialization');
		let modal_text = modal_Clone.querySelector('.modal__text');

		let trainer_Card = event.target.closest('.trainer');
		let search_IMG = trainer_Card.querySelector('.trainer__img').src.split('/');

		for (let el of search_IMG) {
			if (el !== 'img') search_IMG.shift();
			else break;
		}
		search_IMG = './' + search_IMG.join('/');

		let current_Data = null;
		for (let el of filterDATA) {
			if (search_IMG === el.photo) {
				current_Data = el;
				break;
			}
		}

		modal_Name.innerText = `${current_Data["first name"]} ${current_Data["last name"]}`;
		modal_IMG.src = current_Data.photo;
		modal_category.innerText = `Категорія: ${current_Data.category}`;
		modal_experience.innerText = `Досвід: ${current_Data.experience.split(' ')[0]} ${current_Data.experience.split(' ')[0] === '1' ? "рік" : "років"}`;
		modal_specialization.innerText = `Напрям тренера: ${current_Data.specialization}`;
		modal_text.innerText = current_Data.description;

		const modal_Close = modal_Clone.querySelector('.modal__close');
		modal_Close.addEventListener('click', () => {
			const current_Modal = document.querySelector('.modal');
			current_Modal.remove();
		})

		body.append(modal_Clone);
	}

	if (event.target.classList.contains('sorting__btn')) {
		// Зміна активної кнопки
		const sort_btns = event.target.closest('.sorting').querySelectorAll('.sorting__btn');
		sort_btns.forEach((el) => {
			if (el.classList.contains('sorting__btn--active')) el.classList.remove('sorting__btn--active');
		})
		event.target.classList.add('sorting__btn--active');

		// Чистка карток перед сортуванням

		const cards = cardList.querySelectorAll('.trainer');
		cards.forEach((el) => {
			el.remove();
		})

		// Реалізація сортування
		if (event.target.innerText === 'ЗА ЗАМОВЧУВАННЯМ') {
			cardsOut(filterDATA);
		}

		if (event.target.innerText === 'ЗА ПРІЗВИЩЕМ') {
			const sortByLastName = [...filterDATA];

			sortByLastName.sort((a, b) => {
				let fa = a['last name'].toLowerCase(),
					fb = b['last name'].toLowerCase();

				if (fa > fb) {
					return 1;
				}
				if (fa < fb) {
					return -1;
				}
				return 0;
			})

			cardsOut(sortByLastName);
		}

		if (event.target.innerText === 'ЗА ДОСВІДОМ') {
			const sortByExperience = [...filterDATA];

			sortByExperience.sort((a, b) => {
				let ea = a.experience.split(' ')[0].toLowerCase(),
					eb = b.experience.split(' ')[0].toLowerCase();

				if (ea < eb) {
					return 1;
				}
				if (ea > eb) {
					return -1;
				}
				return 0;
			})

			cardsOut(sortByExperience);
		}
	}
})
